import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import _ from 'lodash';

//Components
import Video from './Video';

//Action Creators
import { initOwlCarousel } from '../actions';

class FeaturedVideos extends Component {
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(initOwlCarousel());
    }

    render() {
        const { videos } = this.props;
        const isMobileDevice = this.isMobileDevice();
        return (
            <div id="featured-vids" className="featured-vids-container col-md-10 col-md-offset-1">
                <h1>Featured Videos</h1>
                <div className="featured-videos-blurb">
                    Through her devotion to the artform, Accalia aims to channel the rasa, or, essence, of each dance. Her versatile musicality is a reflection of the many aspects of life and being that she explores through dance
                </div>
                <div className="videos-wrapper">
                    <div className="featured-video-slider owl-carousel owl-theme">
                        {this.renderVideos()}
                    </div>
                </div>
                
                <div className="text-center">Swipe left / right for more videos!</div>

                <a href="#hire" className="btn btn-primary book">Book Now</a>
            </div>
        );
    }

    renderVideos() {
        const { videos } = this.props;
        return _.map(videos, (video) => {
            return <div className="featured-video-element item" key={video.name}><Video video={video} /></div>
        })
    }

    isMobileDevice() {
        return /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    }
}

function mapStateToProps(state) {
    return {
        videos: state.homepage.videos,
    }
}

export default connect(mapStateToProps)(FeaturedVideos);