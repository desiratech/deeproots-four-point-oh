import React from 'react';

const modalOverlay = (props) => {
    return(
        <div id="modal-overlay" onClick={hidePoster}>
            <div id="posterwrapper">
               {props.children}
            </div>
        </div>
    );
}

function hidePoster() {
    const poster = document.getElementById('modal-overlay');
    poster.style.display = 'none';
}

export default modalOverlay;