import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';

import Modal from 'react-modal';

import { openVideo } from '../actions'

class Video extends Component {
    render() {
        const { video } = this.props;
        return (
            <div className="featured-videos">
                <a href={video.url} target="_blank">
                    <img src={"img/" + video.thumbnail + ".png"} alt={video.imgAlt} onClick={this.openVideo.bind(this, video)} />
                    <span className="thumbnail-name">{video.name}</span>
                    <span className="playbutton"><i className="fas fa-play-circle"></i></span>
                </a>

                <Modal
                    isOpen={video.isOpen}
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 className="modal-title">Modal Header</h4>
                        </div>
                        <div className="modal-body">
                            <iframe src={video.url} frameBorder="0" allowFullScreen></iframe>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </Modal>
            </div>
        )
    }

    openVideo(video, evt) {
        const { dispatch } = this.props;
        dispatch(openVideo(video));
    }
}

export default connect()(Video);