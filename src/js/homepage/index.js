import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import cnames from 'classnames';

//components
import Hero from '../common/commonElements/Hero';
import FeaturedVideos from './components/featuredVideos';
import ModalOverlay from './components/modalOverlay';
import ClearfixedContainer from '../common/commonElements/ClearfixedContainer';
import ResponsiveMasonryElement from '../common/commonElements/ResponsiveMasonryElement';

//Action creators
import { handleEmailTextBoxChange, validateEmail } from './actions';

const heroClassNames = {
    intro: ['intro', 'parallax'],
    learn: ['learn', 'parallax']
}
class HomePage extends Component {
    render() {
        const { images } = this.props;
        const { isLoading, validEmail, success, email } = this.props.homepage;
        return (
            <div id="home" className="home-page">
                <Hero classNames={heroClassNames.intro} img="img/bg-hero-home-color.png" >
                    <div className="lazyload be-you-tiful" data-src="/img/logo-white-blue.png" alt="deep roots strong dance blue white logo" ></div>
                </Hero>
                <ClearfixedContainer>
                    <div id="bio" className="one-page-bio">
                        <h1 >Bio</h1>
                        <p>
                            A Dance Manitoba Gold Award recipient and Manitoba Arts Council grant recipient, Accalia is the founder and director of Deep Roots Strong Dance® dance company. She offers bellydance performance, instruction, and directs various dance projects. She performs professionally at weddings and restaurants across Manitoba - and at Festivals across Canada! 
                        </p>

                        <p>
                            Accalia has directed, choreographed and performed for the Canadian Palestinian Association of Manitoba's (CPAM) Arabic-Canadian Heritage Celebration, and produced a number of student and professional shows, including benefit productions, from which all proceeds are donated to The United Nations High Commission for Refugees (UNHCR) and animal welfare organizations. 
                        </p>

                        <p>
                            Dancing in expected as well as unexpected contexts for bellydance; you may see Accalia dancing at a hookah lounge or wedding, however, you may also see her dancing alongside a spoken-word artist, electronic music DJ or head-balancing her fire sword in a festival Fire Show. She has performed with Juno-nominee Adham Shaikh, and numerous musicians across genres.
                        </p>

                        <p>
                            Accalia is a Certified <a href="http://www.rachelbrice.com/what-is-datura-style/" target="_blank">Datura Style™ (Bellydance) practitioner</a>. In 2017, she received a grant from the Manitoba Arts Council to study with world-renowned dancer, Rachel Brice in Portland, Oregon. She became a Certified Datura Style™ practitioner upon her successful completion of this training, the written test, and the subsequent practical test and dance composition project.   
                        </p>

                        <p>
                        As a community leader, Accalia is committed to paying it forward. Since she began teaching in 2012, her special event Bellydance Classes donate all proceeds to charity. All of her dance classes are explicitly LGBTTQ* safe spaces, where all are welcome to participate in a supportive learning environment.
                        </p>
                    </div>
                </ClearfixedContainer>
                <ClearfixedContainer classNames={["bg-seagreen"]}>
                    <FeaturedVideos />
                </ClearfixedContainer>
                <div id="learn"></div>
                <Hero classNames={heroClassNames.learn} img="img/bg-hero-learn.jpg">
                    <div className="learn-blurb">
                        <h2>“When you show up authentic, you create the space
                        for others to do the same.</h2><h2> Dance in your truth.”</h2>

                    </div>
                </Hero>

                <ClearfixedContainer>
                    <div id="hire">
                        <div className="offer">
                            <span className="circle weddings-and-events lazyload" data-src="/img/weddings-and-events.jpg"></span>

                            <h2>Performance</h2>
                            <h3>Weddings and Special Events:</h3>
                            <ul>
                                <li>12 minute performance set: $170</li>
                                <li>20 minute performance set: $220</li>
                            </ul>
                        </div>
                        <div className="offer">
                            <span className="circle classes-and-workshops lazyload" data-src="/img/classes-and-workshops.jpg"></span>
                            <h2>Classes and Workshops</h2>
                            <p>Enter your email address to join the mailing list and be updated about upcoming Dance Workshops, Classes and Lessons with Accalia, as well as with affiliated dance instructors.</p>
                            <input className="email-list-subscriber" onChange={this.emailSubscriberChange.bind(this)} value={email} />
                            <button onClick={this.submitEmail.bind(this)} className="btn btn-primary mailing-list-submit">
                                {
                                    isLoading ?
                                        <div>
                                            <i className={validEmail ? "fa fa-check" : "fa fa-spinner fa-pulse"}></i>
                                            <span className="sr-only">Loading...</span>
                                        </div> :
                                        "Join the List"
                                }
                            </button>
                            <div className={cnames({ success: success, error: !success })}>
                                {this.renderSuccessMessage(success)}
                            </div>
                        </div>
                        <div className="offer">
                            <span className="circle learn-to-bellydance lazyload" data-src="/img/learn-to-bellydance-circle.jpg"></span>
                            <h2>Learn to Bellydance Parties</h2>
                            <h3>Perfect for bachelorettes and birthdays!</h3>
                            <ul>
                                <li>$15 per person for groups of 6-9</li>
                                <li>$12 per person for groups of 10+</li>
                            </ul>
                        </div>

                        <div className="more-info">
                            <h1>
                                Contact: <a href="mailto:info@deeproots-strongdance.com">
                                    <i className="fa fa-envelope"></i> info@deeproots-strongdance.com</a>
                            </h1>

                            <small>Prices are subject to increase during certain holidays (i.e New Years Eve)</small>
                        </div>
                    </div>
                </ClearfixedContainer>

                <ClearfixedContainer>
                    <div id="gallery" className="col-md-8 col-md-offset-2">
                        <ResponsiveMasonryElement />
                    </div>
                </ClearfixedContainer>
                {/* <ModalOverlay/> -> Add children (a poster) */}
            </div>
        );
    }

   

    emailSubscriberChange(evt) {
        const { dispatch } = this.props;
        dispatch(handleEmailTextBoxChange(evt.target.value));
    }

    submitEmail(evt) {
        const { dispatch } = this.props;
        const { email } = this.props.homepage;
        evt.target.disable;
        dispatch(validateEmail(email));
    }

    renderSuccessMessage(success) {

        return success !== null ?
            success ? "Thank you for signing up!" : "We're sorry that couldn't be completed, please try again." :
            null
    }
}

function mapStateToProps(state) {
    return {
        homepage: state.homepage
    }
}

export default connect(mapStateToProps)(HomePage);
