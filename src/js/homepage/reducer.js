import { ActionTypes } from './actions'

let initialState = {
  email: '',
  success: null,
  isLoading: false,
  validEmail: false,
  errors: [],
  videos: [
    {
      url: 'https://www.youtube.com/watch?v=yqxqSjrJsm0',
      name: 'Fusion BellyDance',
      imgAlt: 'FusionBellyDance',
      thumbnail: 'fusionBellyDance',
    },
    // {
    //     url: 'https://www.youtube.com/embed/J1qb_dMnBao',
    //     name: 'Raga',
    //     imgAlt: 'Raga',
    //     thumbnail: 'raga'
    // },
    // {
    //     url: 'https://www.youtube.com/embed/QUPKL5-EEMg',
    //     name: 'The Weeknd',
    //     imgAlt: 'ClubShowWknd',
    //     thumbnail: 'clubShowWknd'
    // },
    // {
    //     url: 'https://www.youtube.com/watch?v=_4hts2bARzM',
    //     name: 'Veil Clip',
    //     imgAlt: 'VeilClip',
    //     thumbnail: 'VeilClip'
    // },
    {
      url: 'https://www.youtube.com/watch?v=aqXjrlcfn-s',
      name: 'Drum Solo',
      imgAlt: 'DrumSolo',
      thumbnail: 'drumSolo',
    },
    {
      url:
        'https://www.youtube.com/watch?v=VSgT-HIGypc&feature=youtu.be&ab_channel=DeepRootsStrongDance',
      name: 'Arabic Rhythm Solo',
      imgAlt: 'Arabic Rhythm Solo',
      thumbnail: 'arabicRhythymThumbnails',
    },
    {
      url: 'https://www.youtube.com/embed/FPe5jE_WMzw',
      name: 'Ellie Goulding Lights',
      imgAlt: 'EllieGouldingLights',
      thumbnail: 'EllieGouldingLights',
    },
    // {
    //     url: 'https://www.youtube.com/watch?v=ya24zq_zDkc',
    //     name: 'Electronic Music Solo',
    //     imgAlt: 'ElectronicMusicSolo',
    //     thumbnail: 'ElectronicMusicSolo'
    // },
    {
      url: 'https://www.youtube.com/embed/vT6T7CgscMA',
      name: "Gangsta's Paradise Remix",
      imgAlt: '1920sGangstaParadise',
      thumbnail: 'gangstasParadise',
    },
    {
      url: 'https://www.youtube.com/embed/RRQlrgtBlIw',
      name: 'Bhangra Music',
      imgAlt: 'BhangraMusic',
      thumbnail: 'bhangraMusic',
    },
  ],
}

function homepage(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.EMAIL_TEXTBOX_CHANGE:
      var newState = state
      newState.email = action.payload

      return Object.assign({}, state, newState)
    case ActionTypes.RECEIVE_RESPONSE:
      var newState = state
      newState.success = true
      newState.validEmail = true

      return Object.assign({}, state, newState)
    case ActionTypes.VALIDATING__AND_SUBMITTING_EMAIL:
      var newState = state
      newState.isLoading = action.payload

      return Object.assign({}, state, newState)
    case ActionTypes.INVALID_EMAIL:
      var newState = state
      newState.success = false
      newState.validEmail = false
      newState.isLoading = false

      return Object.assign({}, state, newState)
    case ActionTypes.LOG_ERRORS:
      var newState = state

      newState.errors = action.payload
      return Object.assign({}, state, newState)
    case ActionTypes.CLEAR_EMAIL_INPUT:
      var newState = state

      newState.email = ''
      return Object.assign({}, state, newState)
    default:
      return state
  }
}

export default homepage
