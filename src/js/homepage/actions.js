import validator from 'email-validator';

export const ActionTypes = {
    "HOME_PAGE_LOADED": "HOME_PAGE_LOADED",
    "EMAIL_TEXTBOX_CHANGE": "EMAIL_TEXTBOX_CHANGE",
    "VALIDATING__AND_SUBMITTING_EMAIL": "VALIDATING__AND_SUBMITTING_EMAIL",
    "INVALID_EMAIL": "INVALID_EMAIL",
    "RECEIVE_RESPONSE": "RECEIVE_RESPONSE",
    "LOG_ERRORS": "LOG_ERRORS",
    "CLEAR_EMAIL_INPUT": "CLEAR_EMAIL_INPUT",
}

export function homepageLoaded() {
    return {
        type: ActionTypes.HOME_PAGE_LOADED,
        payload: {}
    }
}

export function handleEmailTextBoxChange(value) {
    return {
        type: ActionTypes.EMAIL_TEXTBOX_CHANGE,
        payload: value
    }
}

export function initOwlCarousel() {
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })

    return {
        type: "OWLINITED"
    }
}

export function validateEmail(email) {

    return (dispatch, getState) => {

        dispatch(submittingInfo(true));
        if (validator.validate(email)) {
            dispatch(sendToServer(email));
        } else {
            dispatch(invalidEmail());
        }
    }
}

function submittingInfo(success) {
    return {
        type: ActionTypes.VALIDATING__AND_SUBMITTING_EMAIL,
        payload: success
    }
}

function invalidEmail(isValid) {
    return {
        type: ActionTypes.INVALID_EMAIL
    }
}
export function openVideo(videoToOpen) {
    return {
        type: "OPENVIDEOS",
        payload: videoToOpen
    }
}

function sendToServer(email) {
    return (dispatch, getState) => {
        fetch('/api/mailing-list', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(response => { return response.json(); })
            .then(data => {
                if (data.errors) {
                    dispatch(invalidEmail());
                    dispatch(logErrors({ "sendEmail:failed": data.errors }));
                } else {
                    dispatch(receiveFromServer(data.success));
                    dispatch(clearEmailInput());
                }
            })
            .catch(err => {
                dispatch(logErrors(err));
            });
    };
}

function receiveFromServer(response) {
    return {
        type: ActionTypes.RECEIVE_RESPONSE,
        payload: response
    };
}

function clearEmailInput() {
    return {
        type: ActionTypes.CLEAR_EMAIL_INPUT,
    };
}

function logErrors(errors) {
    return {
        type: ActionTypes.LOG_ERRORS,
        payload: {
            errors
        }
    };
}
