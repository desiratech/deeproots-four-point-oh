export const genericActionTypes = {
    "PAGE_LOADED": "PAGE_LOADED",
    "RETRIEVE_SITE_DATA": "RETRIEVE_SITE_DATA",
    "ERROR_STATE": "ERROR_STATE"
}

