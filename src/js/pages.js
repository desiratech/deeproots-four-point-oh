//Custom Pages
import HomePage from './homepage/index';
import Gallery from './gallery/index';
import Learn from './learn/index';
import Schedule from './schedule/index';
import Bookings from './bookings/index';
import Bio from './bio/index';

 const Pages = {
    '/': HomePage,
    '/home': HomePage,
    '/bio': Bio,
    '/learn': Learn,
    '/schedule': Schedule,
    '/book': Bookings,
    '/gal': Gallery
}

export default Pages;