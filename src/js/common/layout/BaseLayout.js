import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect
} from 'react-router-dom';
//BaseLayout
import Navigation from '../navigation/Navigation';
import Footer from '../footer/Footer';
import ClearfixedContainer from '../commonElements/ClearfixedContainer';

import Pages from '../../pages';

class BaseLayout extends Component {
    componentDidMount() {
    }

    render() {

        return (
            <Router>
                <Route render={({ location }) => (
                    <div className="base-container">
                        <Route exact path="/"  />
                        <ClearfixedContainer>
                            <Navigation classNames={['top-nav']} float="right" />
                            <ReactCSSTransitionGroup
                                transitionName="fade"
                                transitionEnterTimeout={300}
                                transitionLeaveTimeout={.01}
                            >
                                <Route
                                    location={location}
                                    key={location.key}
                                    path={location.pathname}
                                    component={Pages[location.pathname]}
                                />

                            </ReactCSSTransitionGroup>
                        </ClearfixedContainer>
                        {this.renderFooter()}
                    </div>
                )}
                />
            </Router>
        );
    }

    renderFooter() {
        return typeof this.props.footer !== 'undefined' ? <Footer {...this.props.footer} /> : null;
    }

}

function mapStateToProps(state) {
    return {
        global: state.global,
        errors: state.errors,
        navigation: state.navigation,
        footer: state.footer
    }
}
export default connect(mapStateToProps)(BaseLayout);