import React, { Component } from 'react';
import { connect } from 'react-redux';
import { render } from 'react-dom';
import _ from 'lodash';

import Event from './components/Event';

class Schedule extends Component {
    componentDidMount() {
    }

    render() {
        const { events } = this.props;
        
        let eventList = _.map(events, event => {
            return <Event key={Math.random()} {...event} />
        });

        return <div>{eventList}</div>

    }

    renderEvents() {
        
        return 
    }
}

function mapStateToProps(state) {
    return {
        events: state.events
    }
}
export default connect(mapStateToProps)(Schedule);