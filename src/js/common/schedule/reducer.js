import Immutable from 'immutable';

let initialState = {
           events: [
        {
            title: "David is Best event.",
            date: "May 9th 2017",
            time: "8PM-2AM",
            location: "226 3rd St North",
            description: "Party party yah!"
        },
    ],
}

function events(state = initialState.events, action) {
    switch (action.type) {
        // case NavigationActionTypes.CLICK_NAV:
        //     return Immutable.fromJS(state).mergeDeep({ currentPage: action.pageUrl }).toJS();
        // case NavigationActionTypes.RENDERED_NAV:
        //     return Immutable.fromJS(state).mergeDeep({ firstRender: action.payload }).toJS();
        default:
            return state;
    }

    return state;
}

export default events