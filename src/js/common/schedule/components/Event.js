import React, { Component } from 'react';
import { render } from 'react-dom';
import EventModal from './EventModal';

class Event extends Component {

    componentDidMount() {
    }

    render() { 
        const {title, date, time, location, description} = this.props.event;
            
        return (
        <div className="event" onClick={this.renderEventModal.bind(this)}>
                <h1 className="-name">{title}</h1>
                <div className="-date">{date}</div>
                <div className="-time">{time}</div>
                <div className="-location">{location}</div>
                <div className="-description">{description}</div>
            </div>
        );
    }

    renderEventModal(evt) {
        return (
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                        BLAH!
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        )
    }

}

export default Event;