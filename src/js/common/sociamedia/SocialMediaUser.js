import React, { Component } from 'react';
import { render } from 'react-dom';

class SocialMediaUser extends Component {

    componentDidMount() {
    }

    render() {
        const { user } = this.props;
        return <a href={user.platformUrl} target="_blank">
            <i className={user.icon}></i>
        </a>
    }
}

export default SocialMediaUser;