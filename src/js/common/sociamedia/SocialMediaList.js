import React, { Component } from 'react';
import { render } from 'react-dom';
import SocialMediaUser from './SocialMediaUser';
import _ from 'lodash';

class SocialMediaList extends Component {
    render() {
        return (
            <div className="social-media">
                {this.renderSocialMediaList()}
            </div>
        );
    }

    renderSocialMediaList() {
        const {socialMedia} = this.props;
        return _.map(socialMedia, user => {
           return <SocialMediaUser user={user} />
        });
    }
}

export default SocialMediaList;