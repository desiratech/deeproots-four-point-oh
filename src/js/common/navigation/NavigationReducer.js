import { NavigationActionTypes } from './NavigationActions';
import Immutable from 'immutable';

let initialState = {
    navigation: {
        showMobileMenu: false,
        currentPage: "home",
        items: [
            {
                name: "Home",
                url: "#home",
            },
            {
                name: "Bio",
                url: "#bio",
            },

            // {
            //     name: "Schedule",
            //     url: "schedule",
            // },
            // {
            //     name: "Blog",
            //     url: "blog",
            // },
            {
                name: "Videos",
                url: "#featured-vids",
            },
            {
                name: "Learn",
                url: "#learn",
            },
            {
                name: "Hire",
                url: "#hire",
            },
            {
                name: "Gallery",
                url: "#gallery",
            },
            {
                name: "Classes",
                url: "/classes",
                targetBlank: true
            },
            // {
            //     name: "BUY TICKETS",
            //     url: "https://www.eventbrite.ca/e/courage-curiosity-dances-from-around-the-world-tickets-51343719447",
            //     targetBlank: true
            // }
        ]
    },
}

function navigation(state = initialState.navigation, action) {
    switch (action.type) {
        case NavigationActionTypes.CLICK_NAV:
            return Immutable.fromJS(state).mergeDeep({ currentPage: action.pageUrl }).toJS();
        case NavigationActionTypes.SHOW_MOBILE_MENU:
            return Immutable.fromJS(state).mergeDeep({ showMobileMenu: true }).toJS();
        case NavigationActionTypes.HIDE_MOBILE_MENU:
            return Immutable.fromJS(state).mergeDeep({ showMobileMenu: false }).toJS();
        default:
            return state;
    }
}

export default navigation