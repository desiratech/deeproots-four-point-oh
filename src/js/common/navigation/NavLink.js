import React from 'react';
import { Link } from 'react-router-dom'

const NavLink = (props) => (
    <li className="nav-item">
        <a href={props.to} onClick={props.onClickHandler} target={props.targetBlank? "_blank" : ""}>{props.children}</a>
    </li>
);

export default NavLink;
