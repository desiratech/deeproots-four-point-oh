export const NavigationActionTypes = {
    "CLICK_NAV": "CLICK_NAV",
    "SHOW_MOBILE_MENU": "SHOW_MOBILE_MENU",
    "HIDE_MOBILE_MENU": "HIDE_MOBILE_MENU"
}

export function handleNavigationChange(pageUrl) {
    return {
        type: NavigationActionTypes.CLICK_NAV,
        pageUrl
    }
}

export function toggleMobileMenu(showMobileMenu) {
    return showMobileMenu === true ? {
        type: NavigationActionTypes.HIDE_MOBILE_MENU
    } : {
        type: NavigationActionTypes.SHOW_MOBILE_MENU
    }
}