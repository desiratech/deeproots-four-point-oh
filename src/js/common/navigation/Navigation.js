import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import Nav from './components/Nav'

//Actions
import { handleNavigationChange, toggleMobileMenu } from './NavigationActions';

class Navigation extends Component {
    render() {
        const { nav, classNames, float } = this.props;
        
        return (
            <div className="nav-wrapper">
                <div className="logo hidden-md-up">
                    <a href="/" className="logo-link">
                        <span className="logo-text">Deep Roots  &#2384;  Strong Dance</span>
                    </a>
                </div>
                <span className="hamburger hidden-md-up" onClick={this.toggleMobileMenu.bind(this)}>
                    <i className="fa fa-bars"></i>
                </span>
                {this.renderNavBar()}
            </div>
        );
    }

    onNavClick(url) {
        const { dispatch } = this.props;
        dispatch(handleNavigationChange(url));
    }

    toggleMobileMenu() {
        const { dispatch, showMobileMenu } = this.props;
        dispatch(toggleMobileMenu(showMobileMenu));
    }

    renderNavBar() {
        const { showMobileMenu, nav } = this.props;
        const isMobile = this.isMobileDevice();
        if(!isMobile) {
            return <Nav nav={nav} />
        } else {
            return showMobileMenu ? <Nav nav={nav} isMobile={isMobile}/> : null;
        }
    }

    isMobileDevice() {
        return /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    }
    
}

function mapStateToProps(state) {
    return {
        nav: state.navigation,
        showMobileMenu: state.navigation.showMobileMenu
    }
}

export default connect(mapStateToProps)(Navigation);