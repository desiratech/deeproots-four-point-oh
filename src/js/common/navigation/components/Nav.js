import React, { Component } from 'react';
import _ from 'lodash';
import cnames from 'classnames';
import { connect } from 'react-redux';
import {toggleMobileMenu} from '../NavigationActions';
import NavLink from '../NavLink';
import SocialMediaList from '../../sociamedia/SocialMediaList';



class Nav extends Component {
        
    hideMenu() {
        const { isMobile, dispatch } = this.props;
        if(isMobile) {
         dispatch(toggleMobileMenu(true));
        }
     }


    render() {
        const { nav, classNames, float, isMobile } = this.props;
        const socialMedia = [
            {
                platformUrl: 'https://www.facebook.com/DeepRootsStrongDance',
                icon: 'fab fa-facebook-square'
            },
            {
                platformUrl: 'https://www.instagram.com/accalia_dance/',
                icon: 'fab fa-instagram'
            },
            {
                platformUrl: 'https://www.youtube.com/user/5calia',
                icon: 'fab fa-youtube'
            },
            // {
            //     platformUrl: 'https://twitter.com/accalia_dance',
            //     icon: 'twitter'
            // },
            {
                platformUrl: 'mailto:info@deeproots-strongdance.com',
                icon: 'fas fa-envelope'
            },
        ];

        return (
            <div className={cnames('nav-bar', { mobile: isMobile })}>
                <div className="logo hidden-sm-down">
                    <a href="/" className="logo-link">
                        <div className="logo-text">Deep Roots &#2384;</div>
                        <div className="logo-text">Strong Dance</div>
                    </a>
                </div>
                <nav className={cnames(['nav', classNames])} >
                    <div>
                        {
                            _.map(nav.items, (navItem) => {
                                return (
                                    <NavLink key={navItem.name} to={navItem.url} targetBlank={navItem.targetBlank} onClickHandler={this.hideMenu.bind(this)}>
                                        {navItem.name.toUpperCase()}
                                    </NavLink>
                                );
                            })
                        }
                    </div>
                    <SocialMediaList socialMedia={socialMedia} />
                </nav>
            </div>
        );
    }
}

export default connect()(Nav);