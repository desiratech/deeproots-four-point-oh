import React from 'react';
import classnames from 'classnames';

const Image = (props) => {
    const {image} = props;

    return <img classNames={classnames(["image", image.classNames])} src={image.src} alt={image.alt} title={image.title} />
}