import Immutable from 'immutable';

let initialState = {
    showModal: false,
    src: '',
    images: window.pictures
}

function masonry(state = initialState, action) {
    switch (action.type) {
        case "SHOW_MODAL":
            return Immutable.fromJS(state).mergeDeep({ showModal: true, src: action.payload.src, description: action.payload.description }).toJS();
        case "HIDE_MODAL":
            return Immutable.fromJS(state).mergeDeep({ showModal: false, src: '' }).toJS();
        default:
            return state;
    }
}

export default masonry