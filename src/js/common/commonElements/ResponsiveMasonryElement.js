import React, { Component } from "react";
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry';
import _ from 'lodash';
import { connect } from 'react-redux';
import { showModal, hideModal, getImages } from './MasonryActions';
// The number of columns change by resizing the window 
class ResponsiveMasonryElement extends Component {

	constructor(props) {
		super(props);
	}

	componentWillMount() {
		const { dispatch } = this.props;
		dispatch(getImages());
	}
	
	render() {
		const { masonry } = this.props;
		return (
			<div className="responsiveMasonryElement">
				<ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 750: 2, 900: 3 }}>
					<Masonry gutter="3">
						{this.renderImages()}
					</Masonry>
				</ResponsiveMasonry>

				{masonry.showModal ? this.renderModal() : null}
			</div>
		)
	}

	expandImage(evt) {
		const { dispatch } = this.props;
		dispatch(showModal(evt.target.dataset.pic, evt.target.dataset.description));
	}

	renderModal() {
		const { masonry } = this.props;

		return <div className="masonry-modal-container" onClick={this.hideModal.bind(this)}>
			<div className="content gal-image-wrapper">
				<img src={masonry.src} />
				{masonry.description ? <span className="gal-description">{masonry.description}</span> : null}
			</div>
		</div>
	}

	hideModal() {
		const { dispatch } = this.props;
		dispatch(hideModal());
	}

	renderImages() {
		const { masonry } = this.props;
		return _.map(masonry.images, image => {
			return <img className="lazyload" data-src={image.src} data-pic={image.src} key={image.src} alt={image.description}  data-description={image.description} style={{ width: "100%", display: "block" }} onClick={this.expandImage.bind(this)} />
		});
	}

}


function mapStateToProps(state) {
	return {
		masonry: state.masonry,
		images: state.images
	}
}

export default connect(mapStateToProps)(ResponsiveMasonryElement);


/*// The number of columns don't change by resizing the window 
class Static extends Component {
	render(){
		const {images} = props;
		return (
			<Masonry columnsCount={3}>
				{images.map((image, i) =>
					<img key={i} src={image.url} alt={image.alt} title={image.title} style={{width: "100%", display: "block"}} />
				)}
			</Masonry>
		)
	}
}*/