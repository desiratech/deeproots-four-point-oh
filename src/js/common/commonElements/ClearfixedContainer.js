import React, { Component } from 'react';
import { render } from 'react-dom';
import classnames from 'classnames';

const ClearfixedContainer = (props) => {
    const {classNames, children} = props;
    return (
        <div className={classnames(['clearfixed-container', classNames])}>
            {children}
        </div>
    );
}

export default ClearfixedContainer;