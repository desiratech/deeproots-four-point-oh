import { getJSON } from '../Helpers';

export function showModal(imageSrc, imageDescription) {
    return {
        type: "SHOW_MODAL",
        payload: {
            src: imageSrc,
            description: imageDescription
        }
    }

}

export function hideModal() {
    return {
        type: "HIDE_MODAL"
    }
}

export function getImages() {
    return (dispatch) => {
        getJSON('api/photo-gal-images')
        .then( (data) => {
            dispatch(receiveImages(data));
        })
    };
}

function receiveImages(data) {
    return {
        type: "RECEIVE_GAL_IMAGES",
        payload: data
    }
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return response
    } else {
      var error = new Error(response.statusText)
      error.response = response
      throw error
    }
  }
  
  function parseJSON(response) {
    return response.json()
  }
  