import React from 'react';
import classnames from 'classnames';

import Image from './Image';

const Logo = (props) => {
    const {img, classNames} = props;

    return (
        <div className={classnames(["logo", classNames])}>
            <Image image={img}/>
        </div>
    )
}