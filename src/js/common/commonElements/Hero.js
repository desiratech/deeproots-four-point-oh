import React, { Component } from 'react';
import { render } from 'react-dom';
import classnames from 'classnames';

const Hero = (props) => {
    const {img, msg, classNames, children} = props;
    const backgroundImage = {
        'backgroundImage': 'url('+img+')',
    }
    return (
        <div className={classnames(['hero', 'lazyload', classNames])} data-src={img}>
            {children}
        </div>
    );
}

export default Hero;