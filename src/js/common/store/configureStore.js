import {createStore, applyMiddleware, compose} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

export default function configureStore(rootReducer, initialState) {
    return createStore(rootReducer, initialState, composeEnhancers(
        getEnhancer()
    ));
}


const composeEnhancers = composeWithDevTools({});
function getEnhancer() {
    return compose(applyMiddleware(thunk));
}
