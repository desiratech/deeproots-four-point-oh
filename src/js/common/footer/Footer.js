import React, { Component } from 'react';
import { render } from 'react-dom';

class Footer extends Component {
    render() {
        const { children, copyright_name } = this.props;
        const today = new Date();
        const thisYear = today.getFullYear();
       

        return (
            <footer className="footer">
                {children}
                <span className="copyright">
                    &copy; {thisYear} {copyright_name}
                </span>
                <span className="photo-cred">
                    <a href="http://www.pepperapple.com/" target="blank">Hero Photo: Shawn Fillion</a>
                </span>
            </footer>
        );
    }
}

export default Footer;