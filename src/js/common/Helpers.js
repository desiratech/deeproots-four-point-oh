import 'whatwg-fetch';
/** DOM Helpers */
export function addClass(target, className) {
    target.classList.add(className);
}

export function removeClass(target, className) {
    target.classList.remove(className);
}

export function hasClass(target, className) {
    return target.classList.contains(className);
}

/** API Helpers */

export function getJSON(uri) {
    return fetch(uri)
        .then(checkStatus)
        .then(parseJSON)
        .then((data) => {
            return data;
        })
        .catch((ex) => {
            return { error: ex };
        });
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        var error = new Error(response.statusText);
        error.response = response;
        throw error;
    }
}

function parseJSON(response) {
    return response.json();
}

