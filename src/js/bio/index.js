import React, { Component } from 'react';
import { render } from 'react-dom';

class Bio extends Component {
    render() {
        return (
            <section id="Bio">
                <p><span>Accalia fell in love with fusion bellydance</span></p>
                <p><span> one beautiful summer night&nbsp;</span><span>in the Winnipeg Folk Festival </span></p>
                <p><span>campground at Birds Hill Provincial Park...</span></p>
                <p>&nbsp;</p>
                <p>THE BEGINNINGS&nbsp;</p>
                <p><span>&nbsp; &nbsp; &nbsp;At 19, she was captivated by the strong, sinuous movements of the duo, GaiaPente, who were illuminated by the moonlight and the flames of fire dancers. She had never taken interest in bellydance until this moment. </span></p>
                <p><span>&nbsp; &nbsp; &nbsp;She would explain fusion bellydance to someone who has never seen it as&nbsp;"bellydance's badass sister". Accalia reveres the dance style&nbsp;as a living artform&nbsp;- ever-challenging, and&nbsp;always full of fresh possibility. She&nbsp;began taking lessons with Tara of GaiaPente not long&nbsp;after she first saw fusion bellydance performed on that summer night in 2006.</span></p>
                <p>&nbsp;</p>
                <p>ONGOING DANCE EDUCATION</p>
                <p><span>&nbsp;&nbsp;&nbsp;&nbsp;Accalia has been practicing fusion bellydance for 9 years, studying in Winnipeg and at workshops in San Fransisco &amp;&nbsp;Sebastopol, California; Phoenix, Arizona; and in Portland, Oregon where</span><span>&nbsp;she completed&nbsp;Rachel Brice's 8 Elements™ Phase 1: Initiation with recognition.&nbsp;&nbsp;</span></p>
                <p>&nbsp;</p>
                <p><span>PROFESSIONAL PERFORMANCE &amp; INSTRUCTION &nbsp; &nbsp;</span></p>
                <p><span>&nbsp; &nbsp; </span><span >Accalia's bellydance performance work largely incorporates urban electronic, RnB, Indian, contemporary&nbsp;Middle Eastern, and lyrical music. She has performed with various electronic music DJs including Santa Fe's downtempo duo, Desert Dwellers at Shambhala&nbsp;&amp;&nbsp;The&nbsp;Manitoba Electronic Music Exhibition (MEME); DJ Vilify (Montréal); and Juno and Emmy Award-nominated musician &amp; DJ, Adham Shaikh.&nbsp;Accalia performs at all kinds of special events, restaurants and festivals. &nbsp;</span></p>
                <p >&nbsp; &nbsp; When Accalia began&nbsp;instructing&nbsp;full session classNamees in 2012, she&nbsp;hosted&nbsp;Manitoba's first explicitly LGBT* allied bellydance classNamees. She continues to&nbsp;cultivate a safe&nbsp;space for all people to learn bellydance;&nbsp;her classNamees allow for learning and discovery.&nbsp;</p>
                <p>&nbsp;</p>
                <p>FOUNDATIONS &nbsp; &nbsp;&nbsp;</p>
                <p>&nbsp; &nbsp; &nbsp;The tenets of her dance practice include yoga, resistance training and core strenghening exercises, various technique drills, online dance practice resources, and collaboration&nbsp;within the bellydance community.&nbsp;&nbsp;Accalia has collaborated with several local performing artists, including Patron of Dreams, GaiaPente, LunaSol, Sidramara, and the fire dance group, Prairie Sol.&nbsp;</p>
                <p>&nbsp;</p>
                <p>DANCE STYLE</p>
                <p><span>&nbsp; &nbsp; &nbsp;She is fascinated with the interplay of dancer and space&nbsp;- the ability to&nbsp;shape mood, air&nbsp;texture - and to infuse the space with a certain feeling. Her style is characterized by staccato to silky textures, isolations, pops and locks, and choreographed as well as improvised group and solo performances. Props include fire sword, palm torches, and fire hip belt.&nbsp;</span></p>
                <p><span>&nbsp; </span></p>
                <p><span>ADDITIONAL PASSIONS &nbsp;</span></p>
                <p><span>&nbsp; &nbsp; &nbsp;Accalia loves writing, long-distance running, spending time with human and nonhuman friends, including her cats, Mortimer and Pablo. She holds a B.A. (Honours)&nbsp;in&nbsp;Political Science, with a focus on International Relations in the Middle East. Accalia is an avid animal welfare advocate. She has raised funds for the UNHCR &amp; Mercy For Animals Canada through her Karma Bellydance classNamees, which also promote a plant-based diet and compassionate lifestyle.&nbsp;</span></p>
            </section>
        );
    }
}

export default Bio;