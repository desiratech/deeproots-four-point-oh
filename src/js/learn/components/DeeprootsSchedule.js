import React, {Component} from 'react';
import {render} from 'react-dom';
import _ from 'lodash';

import Schedule from '../../common/schedule/Schedule';

class DeeprootsSchedule extends Component {

    componentDidMount() {
    }

    render() { 
        const {events} = this.props;
        return (
            <Schedule {...events} />
        );
    }
}

export default DeeprootsSchedule;