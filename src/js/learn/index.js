import React, { Component } from 'react';
import { render } from 'react-dom';
import DeeprootsSchedule from './components/DeeprootsSchedule';

class Learn extends Component {

    componentDidMount() {
    }

    render() {
        return (
            <div className="learn-container">
                <DeeprootsSchedule {...this.props} />
            </div>
        );
    }
}

export default Learn;