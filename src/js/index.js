import React, { Component } from 'react';
import { render } from 'react-dom';
//Reducers
import configureStore from './common/store/configureStore';
import rootReducer from './rootReducer';

//Components
import Root from './common/bootstrap/Root';
import BaseLayout from './common/layout/BaseLayout';

//CSS
import 'normalize.css';
import '../sass/main.scss';

let initialState = {
    footer: 
    {
        content: "Stuff for the footer",
        copyright_name: "Deep Roots Strong Dance"
    },
   
};



const store = configureStore(rootReducer, initialState);

class BoilerPlate extends Component {
    render() {
        return (
            <BaseLayout {...store} />
        );
    }
}

render(
    <Root store={store}>
        <BoilerPlate />
    </Root>,
    document.getElementById('app')
);