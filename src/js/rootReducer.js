import { combineReducers } from 'redux';
import { genericActionTypes } from './actions/genericActions';
import _ from 'lodash';
import Immutable from 'immutable';


//reducers
import navigation from './common/navigation/NavigationReducer';
import footer from './common/footer/FooterReducer';
import events from './common/schedule/reducer';
import homepage from './homepage/reducer';
import masonry from './common/commonElements/MasonryReducer';

let initialState = {
    global: {
        currentPage: 'home'
    },

    errors: {},
};

function errors(state = initialState.errors, action) {
    switch (action.type) {
        default:
            break;
    }
    return state;
}

function global(state = initialState.global, action) {
    switch (action.type) {
        default:
            break;
    }
    return state;
}

const rootReducer = combineReducers({
    global,
    errors,
    navigation,
    footer,
    events,
    homepage,
    masonry
});

export default rootReducer;