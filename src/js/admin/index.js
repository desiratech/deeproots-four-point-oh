import React, { Component } from 'react';
import { render } from 'react-dom';

//Components
import NavBar from './components/NavBar';
import Card from './components/Card';
import Datalist from './components/Datalist/Datalist';
import Modal from './components/Modal';
import EditShowForm from './components/Form/Form';
import AddShowForm from './components//Form/Form';
import Button from './components/Button/Button';

//Layout
import Column from './components/Layout/Columns';
import Container from './components/Layout/Container';

//CSS
import 'normalize.css';
import '../../sass/admin/admin.scss';

//Helpers
import ApiRequests from './Helpers/apiRequests';

const initialState = {
    isOpen: false,
    editShow: false,
    isDeleteDialogOpen: false,
    itemToEdit: {
        name: '',
        links: {
            facebook: '',
            eventbrite: ''
        },
        description: '',
        date: new Date(),
        show_id: ''
    },
    shows: [
        {
            show_id: 1,
            name: "Courage & Curiousity",
            date: new Date("Jan-19-2019"),
            description: "description",
            links: {
                facebook: "dasFBELink",
                eventbrite: "dasFBELink2"
            },
        },
        {
            show_id: 2,
            name: "Porage & Furiousity",
            date: new Date("Mar-21-2020"),
            description: "description",
            links: {
                facebook: "dasEBLink",
                eventbrite: "dasEBLink2"
            },
        }
    ]
};

class BoilerPlate extends Component {
    constructor(props) {
        super(props);
        this.onEditClick = this.onEditClick.bind(this);
        this.onDeleteClick = this.onDeleteClick.bind(this);
        this.onShowDateChange = this.onShowDateChange.bind(this);
        this.onCreateNewClick = this.onCreateNewClick.bind(this);
        this.textBoxChangeHandler = this.textBoxChangeHandler.bind(this);

        this.state = initialState;
    }

    render() {
        const { editShow, shows } = this.state;
        return (
            <React.Fragment>
                <NavBar />
                <Container fluid={true}>
                    <div className="row">

                        <Column
                            columnSize="lg"
                            columnNumber={6}
                        >
                            <Card
                                title="Shows"
                                actions_title="Add Shows Here"
                                actions_subtitle="Add all the details for your shows to display on the site"
                                actions={this.renderActions('shows')}
                            >
                                <Datalist
                                    items={shows}
                                    onEditClick={this.onEditClick}
                                    onDeleteClick={this.onDeleteClick}
                                />
                                <Modal
                                    isOpen={this.state.isOpen}
                                    onCloseModal={this.closeModal.bind(this)}
                                    onSaveClick={this.onSaveClick.bind(this)}
                                >
                                    {editShow ? <EditShowForm
                                        id="editShowForm"
                                        itemToEdit={this.state.itemToEdit}
                                        textBoxChangeHandler={this.textBoxChangeHandler}
                                        onDateChangeHandler={this.onShowDateChange} /> :
                                        <AddShowForm
                                            id="addShowForm"
                                            textBoxChangeHandler={this.textBoxChangeHandler}
                                            itemToEdit={this.state.itemToEdit}
                                            onDateChangeHandler={this.onShowDateChange} />}
                                </Modal>
                            </Card>
                        </Column>
                        <Column
                            columnSize="lg"
                            columnNumber={6}
                        >
                            <Card title="Classes">
                                coming soon!
                            </Card>
                        </Column>
                        <Column
                            columnSize="lg"
                            columnNumber={6}
                        >
                            <Card title="Images">
                                coming soon!
                            </Card>
                        </Column>
                        <Column
                            columnSize="lg"
                            columnNumber={6}
                        >
                            <Card title="Social Media">
                                coming soon!
                            </Card>
                        </Column>
                    </div>
                    <Modal
                        isOpen={this.state.isDeleteDialogOpen}
                        onCloseModal={this.closeModal.bind(this)}
                        onSaveClick={this.onOkDeleteClick.bind(this)}
                        type="confirm"
                    >
                        Are you sure you want to delete?
                    </Modal>
                </Container>
            </React.Fragment>
        );
    }

    onEditClick(item) {
        this.setState({
            isOpen: true,
            itemToEdit: item,
            editShow: true
        });
    }

    onCreateNewClick() {
        this.setState({
            isOpen: true,
            itemToEdit: initialState.itemToEdit,
            editShow: false
        });
    }

    onShowDateChange(item, date) {
        item.date = date;
        this.setState({
            isOpen: true,
            itemToEdit: item
        });
    }

    onDeleteClick(id) {
        this.setState({
            isDeleteDialogOpen: true,
            itemToEdit: { show_id: id }
        });
    }

    closeModal() {
        this.setState({
            isOpen: false ,
            isDeleteDialogOpen: false,
            itemToEdit: initialState.itemToEdit
        });
    }

    onOkDeleteClick() {
        const { itemToEdit } = this.state;
        this.deleteShow(itemToEdit);
        this.setState({ isDeleteDialogOpen: false, itemToEdit: initialState.itemToEdit });
    }

    onSaveClick() {
        const { editShow } = this.state;
        const id = editShow ? "#editShowForm" : "#addShowForm";
        const form = document.querySelector(id);
        const formData = new FormData(form);
        let formEntries = {};
        for (var [key, value] of formData.entries()) {

            if (
                key !== 'year' &&
                key !== 'month' &&
                key !== 'hour12' &&
                key !== 'day' &&
                key !== 'minute'
            ) {
                formEntries[key] = value;
            }
        }
        editShow ? this.updateShow(formEntries)`` : this.createShow(formEntries);
    }

    createShow(formEntries) {
        ApiRequests.post('api/admin/shows/create', formEntries)
            .then(data => this.setState({
                items: data.items
            }))
            .catch(err => console.error(err.message));
    }

    updateShow(formEntries) {
        ApiRequests.post(`api/admin/shows/${formEntries.show_id}/update`, formEntries)
            .then(data => this.setState({
                items: data.items
            }))
            .catch(err => console.error(err.message));
    }

    deleteShow(item) {
        ApiRequests.delete(`api/admin/shows/${item.show_id}/delete`)
            .then(data => this.setState({
                items: data.items
            }))
            .catch(err => console.error(err.message));
    }

    renderActions(cardTitle) {
        switch (cardTitle) {
            case 'shows':
                return <Button
                    primary
                    onClickHandler={this.onCreateNewClick}
                >Add Show</Button>
            default:
                break;
        }
    }

    textBoxChangeHandler(evt) {
        switch (true) {
            case evt.target.dataset.show === "1":
                let editedItem = this.state.itemToEdit;
                if (evt.target.id === 'facebook') {
                    editedItem.links.facebook = evt.target.value;
                } else if (evt.target.id === 'eventbrite') {
                    editedItem.links.eventbrite = evt.target.value;
                } else {
                    editedItem[evt.target.id] = evt.target.value;
                }
                this.setState(Object.assign(this.state.itemToEdit, editedItem));
                break;
        }
    }
}

render(
    <div>
        <BoilerPlate />
    </div>,
    document.getElementById('admin')
);