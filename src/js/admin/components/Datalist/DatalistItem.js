import React from 'react';
import IconButton from '../Icon/IconButton';
import Icon from '../Icon/FontAwesomeIcon';

const DatalistItem = (props) => (
    <tr className="table-active">
        {Object.keys(props.item).map(function (key, index) {
            if(key !== 'show_id') {

                switch (key) {
                    case 'links': 
                    return <td>{renderLinks(props.item[key])}</td>;
                    case 'date':
                    return <td>{props.item[key].toString()}</td>
                }
    
                return <td>{props.item[key]}</td>
            }
        })}
        <td className="data-list-actions">
            <IconButton
                iconClass="fas fa-pencil-alt"
                onClickHandler={() => props.onEditClick(props.item)}
            />
            <IconButton
                iconClass="fas fa-times"
                onClickHandler={() => props.onDeleteClick(props.item.id)}
            />
        </td>
    </tr>
)

function renderLinks(links) {
    return Object.keys(links).map(function (key, index) {
        switch (key) {
            case 'facebook':
                return <Icon
                    link={links[key]}
                    fontAwesomeClass="fab fa-facebook-square"
                />
            case 'eventbrite':
                return <Icon
                    link={links[key]}
                    fontAwesomeClass="far fa-ticket-alt"
                />
            default:
                return;
        }
    });
}

export default DatalistItem;