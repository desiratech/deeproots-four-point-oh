import React from 'react';
import DatalistItem from './DatalistItem';

class Datalist extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {items} = this.props;

        return (
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Date</th>
                        <th scope="col">Description</th>
                        <th scope="col">Links</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderDatalistItems(items)}
                </tbody>
            </table>)
    }

    renderDatalistItems(items) {
        return items.map(item => <DatalistItem 
            key={JSON.stringify(item)} 
            item={item} 
            onEditClick={this.props.onEditClick}
            onDeleteClick={this.props.onDeleteClick}
            />)
    }
}

export default Datalist;