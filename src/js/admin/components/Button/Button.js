import React from 'react';
import classnames from 'classnames';
const Button = (props) => (
    <button
        type="button"
        className={classnames("btn", getButtonType(props))}
        onClick={props.onClickHandler}
    >
        {props.children}
    </button>
);

function getButtonType(props) {
    let classes = [];
    switch (true) {
        case props.primary:
            props.outline ?
                classes.push('btn-outline-primary') :
                classes.push('btn-primary');
            break;
        case props.secondary:
            props.outline ?
                classes.push('btn-outline-secondary') :
                classes.push('btn-secondary');
            break;
        case props.success:
            props.outline ?
                classes.push('btn-outline-success') :
                classes.push('btn-success');
            break;
        case props.info:
            props.outline ?
                classes.push('btn-outline-info') :
                classes.push('btn-info');
            break;
        case props.warning:
            props.outline ?
                classes.push('btn-outline-warning') :
                classes.push('btn-warning');
            break;
        case props.danger:
            props.outline ?
                classes.push('btn-outline-danger') :
                classes.push('btn-danger');
            break;
        case props.link:
            props.outline ?
                classes.push('btn-outline-link') :
                classes.push('btn-link');
            break;
        case props.disabled:
            classes.push('disabled');
            break;
    }

    return classes;
}

export default Button;