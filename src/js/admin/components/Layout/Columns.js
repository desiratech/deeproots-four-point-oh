import React from 'react';

const Column = (props) => (
    <div className={"col-"+props.columnSize+"-"+props.columnNumber}>
           {props.children}
    </div>
);

export default Column;