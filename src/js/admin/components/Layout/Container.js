import React from 'react';
const Container = (props) => (
    <div className={props.fluid ? "container-fluid" : "container"}>
        {props.children}
    </div>
);

export default Container;