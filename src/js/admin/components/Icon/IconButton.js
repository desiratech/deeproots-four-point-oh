import React from 'react';
import FontAwesomeIcon from './FontAwesomeIcon';

class IconButton extends React.Component {
    render() {
        const { onClickHandler, iconClass, prefixIcon, helpText, buttonText } = this.props;
        return <React.Fragment>
            <button
                onClick={onClickHandler}
            >
                {this.renderIconAndText(prefixIcon, buttonText, iconClass)}
            </button>
            {helpText ? helpText : null}
        </React.Fragment>;
    }

    renderIconAndText(prefixIcon, buttonText, iconClass) {
        return prefixIcon ?
            <React.Fragment>
                <FontAwesomeIcon
                    fontAwesomeClass={iconClass}
                /> {buttonText}
            </React.Fragment> :
            <React.Fragment>
                {buttonText} <FontAwesomeIcon
                    fontAwesomeClass={iconClass} />
            </React.Fragment>
    }
}

export default IconButton;