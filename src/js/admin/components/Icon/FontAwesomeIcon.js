import React from 'react';

const FontAwesomeIcon = (props) => (
    props.link ? 
    <a href={props.link}><i className={props.fontAwesomeClass}></i></a>:
    <i className={props.fontAwesomeClass}></i>
);

export default FontAwesomeIcon;