import React from 'react';

import DateTimePicker from 'react-datetime-picker';

class Form extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { name, links, description, date, show_id } = this.props.itemToEdit;
        return <form id={this.props.id}>
            <div className="form-group">
                <label htmlFor="name">Show Name</label>
                <input
                    type="text"
                    className="form-control"
                    id="name"
                    data-show="1"
                    aria-describedby="show-name"
                    placeholder="BellyDancing In The Cosmos"
                    name="showName"
                    onChange={this.props.textBoxChangeHandler}
                    value={name} />
                <small id="show-name-help" className="form-text text-muted">The name of your show.</small>
            </div>
            <div className="form-group">
                <label htmlFor="facebook">Facebook Event Url</label>
                <input
                    type="text"
                    className="form-control"
                    id="facebook"
                    data-show="1"
                    aria-describedby="facebookEventUrl"
                    name="facebook"
                    placeholder="facebook.com/event/thing"
                    onChange={this.props.textBoxChangeHandler}
                    value={links.facebook} />
                <small id="facebookEventUrlHelp" className="form-text text-muted">URL to the Facebook event.</small>
            </div>
            <div className="form-group">
                <label htmlFor="eventbrite">EventBrite Url</label>
                <input
                    type="text"
                    className="form-control"
                    id="eventbrite"
                    data-show="1"
                    aria-describedby="eventBriteUrl"
                    name="eventbrite"
                    placeholder="eventbrite.com/thing"
                    onChange={this.props.textBoxChangeHandler}
                    value={links.eventbrite} />
                <small id="eventBriteUrlHelp" className="form-text text-muted">URL to the Eventbrite page.</small>
            </div>
            <div className="form-group">
                <label htmlFor="description">Description</label>
                <textarea
                    className="form-control"
                    id="description"
                    data-show="1"
                    placeholder="Describe your event"
                    name="eventDescription"
                    onChange={this.props.textBoxChangeHandler}
                    value={description}></textarea>
            </div>
            <input type="hidden" name="show_id" value={show_id} />
            <div className="form-group">
                <label>Select Date</label>
                <DateTimePicker
                    onChange={date => this.props.onDateChangeHandler(this.props.itemToEdit, date)}
                    value={date}
                    name="eventDate"
                />
            </div>
        </form>;
    }
}

export default Form;