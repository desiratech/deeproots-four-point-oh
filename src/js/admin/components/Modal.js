import React from 'react';
import classnames from 'classnames';
class Modal extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {isOpen, children, type} = this.props;
        const display = isOpen ? "block":"none";
        let style={"display":display, "paddinRight": "15px"}
        return <React.Fragment>
            <div id="exampleModalLive" className={classnames("modal", "fade", {"show" : isOpen})} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style={style}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLiveLabel">Modal title</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.onCloseModal}>
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {children}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.props.onCloseModal}>{type === "confirm" ? "No" : "Close"}</button>
                            <button type="button" className="btn btn-primary" onClick={this.props.onSaveClick}>{type === "confirm" ? "Yes" : "Save"}</button>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>;
    }
}

export default Modal;