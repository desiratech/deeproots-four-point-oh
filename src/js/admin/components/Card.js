import React from 'react';

class Card extends React.Component {
    render() {
    const {title, children, actions_title, actions_subtitle, actions} = this.props;
        return <React.Fragment>
                <div className="card mb-3">
                    <h3 className="card-header">{title}</h3>
                    <div className="card-body actions">
                        <h5 className="card-title">{actions_title}</h5>
                        <h6 className="card-subtitle text-muted">{actions_subtitle}</h6>
                        <div className="card-text">{actions}</div>
                    </div>
                    <div className="card-body children">
                        {children}
                    </div>
                    <div className="card-footer text-muted">
                        <strong>Last Edit:</strong> 2 days ago
                    </div>
                </div>
            </React.Fragment>;
    }
}

export default Card;