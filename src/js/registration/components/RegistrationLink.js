import React from 'react';

const RegistrationLink = (props) => {
    return (
        <a href={props.url} target="blank" >
            <strong>{props.title}</strong> - {props.description} 
        </a>
    )
};


export default RegistrationLink;