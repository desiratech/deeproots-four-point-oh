import React from 'react';
import { render } from 'react-dom';
//CSS
import 'normalize.css';
import '../../sass/registration.scss';
import Registration from './components/Registration';

render(
    <div>
        <Registration />
    </div>,
    document.getElementById('app')
);