import React, { Component } from 'react';
import RegistrationLink from './RegistrationLink';
import moment from 'moment';

class Registration extends Component {
    render() {
        const registrationURLs = {
            beginner: 'https://goo.gl/forms/LtgNJN5Yy72ZNxlt1',
            intermediate: 'https://goo.gl/forms/an18PMNcJMt06GlA3',
            choreo: 'https://goo.gl/forms/JFc3dBwufp0ouL753',
            performance: 'https://docs.google.com/document/d/1PvrPvzY17iffiRoVFuR7m34j15WyYYowt9cCTMJloi0/edit'
        }
        const dontDisplayUrl = this.isBeforeNovemberFirst();
        console.log('dontDisplayUrl', dontDisplayUrl);
        return <div className="registrations">
            <img className="be-you-tiful" src="img/logo-white-blue.png" alt="deep roots strong dance blue white logo"  />
            <h1>Registration for All 2019 Bellydance Classes with Accalia opens Nov. 1, 2018
   </h1>
            <h2>Classes are Sundays at Nafro Studios (109 Pulford St, Winnipeg)
   </h2>
            <h1>2019 Class Schedule:</h1>

            <ul>
                <li>
                    <RegistrationLink
                        url={dontDisplayUrl ? '' : registrationURLs.beginner}
                        title="BEGINNER BELLYDANCE"
                        description="Sundays, 5pm | 12 weeks (Jan. 12 - Mar. 31) $150 Full session ($12.5/ class) or  $17/ per Drop-in"
                    />
                </li>
                <li>
                    <RegistrationLink
                        url={dontDisplayUrl ? '' : registrationURLs.intermediate}
                        title="INTERMEDIATE FUSION BELLYDANCE"
                        description="Sundays, 7pm | 12 weeks (Jan. 12 - Mar. 31) $150 Full session ($12.5/ class) or  $17/ per Drop-in"
                    />
                </li>
                <li>
                    <RegistrationLink
                        url={dontDisplayUrl ? '' : registrationURLs.choreo}
                        title="ADVENTURES IN CHOREOGRAPHY"
                        description="BELLYDANCE WORKSHOP SERIES Sundays, 6pm | 8 weeks (Jan. 12 -  Mar. 3) $100 Full session only, No Drop-ins."
                    />
                </li>

                <li>
                    <RegistrationLink
                        url={dontDisplayUrl ? '' : registrationURLs.performance}
                        title="PERFORMANCE CLASS"
                        description="Sundays, 6pm | 12 weeks (Mar. 10 - May 26) $180 (Advanced, invite-only) *Fee includes seven (7) mandatory Intermediate Drop-ins"
                    />
                </li>
                <h3><a href="/">Back to Deep Roots Strong Dance</a></h3>
            </ul>
        </div>;
    }

    isBeforeNovemberFirst() {
        const today = moment.tz(new Date(), 'America/Winnipeg');
        const novemberFirst = moment("2018-11-01T12:00:00Z");

        return today.isBefore(novemberFirst);
    }
}

export default Registration;