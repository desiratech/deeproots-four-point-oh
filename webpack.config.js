var webpack = require('webpack'),
path = require('path');

var srcPath = path.join(__dirname, '/src/js'),
distPath = path.join(__dirname, '/public');

var ExtractTextPlugin = require("extract-text-webpack-plugin");
console.log("building..");

module.exports = {
devtool: 'cheap-module-source-map',
context: srcPath,
entry: {
    app:'./index.js',
    admin: './admin/index.js',
    registration: './registration/index.js'
},
output: {
    path: distPath,
    library: 'library',
    libraryTarget: 'umd',
    umdNamedDefine: true,
    filename: 'js/[name].js',
},
resolve: {
    modules: ["node_modules"],
},
module: {
    loaders: [
        { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
        { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ },
        {
            test: /\.scss$/,
            loaders: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: ["css-loader", "sass-loader"]
            }),
        },
        {
            test: /\.css$/,
            loaders: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: "css-loader"
            }),
        },
        {
            test: /\.(png|woff|woff2|eot|ttf|svg)$/,
            loader: 'url-loader?limit=100000'
        }
    ]
},
plugins: [
    new ExtractTextPlugin("css/[name].css"),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify('development')
        }
    }),
    new webpack.ProvidePlugin({
        Promise: 'es6-promise-promise', // works as expected
    })
]
};
